from ubuntu
run apt-get update && DEBIAN_FRONTEND="noninteractive" apt-get -y install \
  build-essential git vim \
  autoconf gperf bison flex texinfo wget help2man gawk libtool-bin \
  libncurses5-dev python python-dev unzip python-serial
workdir /src
workdir esp-open-sdk
copy esp-open-sdk .
run CT_EXPERIMENTAL=y CT_ALLOW_BUILD_AS_ROOT=y CT_ALLOW_BUILD_AS_ROOT_SURE=y \
  make VENDOR_SDK=1.5.4 && rm -rf crosstool-NG
env ESP_ROOT=/src/esp-open-sdk PATH="/src/esp-open-sdk:${PATH}"
workdir ..
